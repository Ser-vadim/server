"use strict";
const nodemailer = require("nodemailer");
const ConfigEmail = require("../../config/Config-email");
const { ErrorGeneration } = require("../../Helper/ErrorGeneration");
const Errors = require("../../CONSTANTS/Errors_const");

class Mailer {

  // TODO подумать о логировании почты
  async sendMail(dataMail, doSomethingIfError) {
    try {
      const transporter = await nodemailer.createTransport(ConfigEmail.email);
      return await transporter.sendMail(dataMail);
    } catch (error) {
      await doSomethingIfError();
      throw ErrorGeneration(
          "some problem with email transport, please try latter make registration",
          Errors.customError
      );
    }
  }
}

module.exports = Mailer;

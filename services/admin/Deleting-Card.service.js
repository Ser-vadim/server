const cardRepository = require("../../repositories/Card_Repository/cards.repository");


const deletingCard = async (id) => {
    const deletedCard = await cardRepository.deleteCardById(id);
    return deletedCard
}

module.exports = {deletingCard}

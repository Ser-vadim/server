require("dotenv").config({ path: `./.env.${process.env.NODE_ENV}` });
const Chalk = require("chalk");
const locationRepository = require("../../repositories/Locations_Repository/locations.repository");
const {ErrorGeneration} = require("../../Helper/ErrorGeneration");
const Errors = require("../../CONSTANTS/Errors_const");

const creatingLocation = async (reqBodyCard) => {
  const dublicateCard = await locationRepository.checkDuplicateLocation('id', {
    name: reqBodyCard.name
  });
    if(dublicateCard) throw ErrorGeneration("location exist", Errors.customError, 200);
    const createdLocation = await locationRepository.insertLocationData(reqBodyCard);
    return createdLocation;
};

module.exports = {
  creatingLocation,
};

const Joi = require("joi");
const Chalk = require("chalk");
const {
  REGEX_PASSWORD,
  REGEX_NOT_ONLY_NUMBER,
} = require("../CONSTANTS/Regex_const");
const { ErrorGeneration } = require("../Helper/ErrorGeneration");
const Errors = require("../CONSTANTS/Errors_const");


const CreateAccount_schema = Joi.object({
  nickname: Joi.string()
    .required()
    .regex(REGEX_NOT_ONLY_NUMBER)
    .error((errors) => {
      errors.forEach((error) => {
        switch (error.code) {
          case "any.required":
            error.message = `Nickname should not be a blank`;
            break;
          case "string.empty":
            error.message = `Nickname should not be a blank`;
            break;
          case "string.pattern.base":
            error.message = `Nickname should not be just numbers`;
            break;
          case "string.base":
            error.message = `Nickname should not be just numbers`;
            break;
          default:
            error.message = "Validation error";
            break;
        }
      });
      return ErrorGeneration(errors[0].message, Errors.ValidationError);
    }),
  password: Joi.string()
    .required()
    .regex(REGEX_PASSWORD)
    .error((errors) => {
      errors.forEach((error) => {
        switch (error.code) {
          case "any.required":
            error.message = `${error.local.key} should not be a blank`;
            break;
          case "string.pattern.base":
            error.message =
              "Password must be at least 8 characters long and contain numbers and one capital letter";
            break;
          default:
            break;
        }
      });
      return ErrorGeneration(errors[0].message, Errors.ValidationError);
    }),
  email: Joi.string()
    .email()
    .required()
    .error((errors) => {
      errors.forEach((error) => {
        switch (error.code) {
          case "any.required":
            error.message = `${error.local.key} should not be a blank`;
            break;
          case "string.Mailer":
            error.message = `${error.local.key} is not valid`;
            break;
          default:
            break;
        }
      });
      return ErrorGeneration(errors[0].message, Errors.ValidationError);
    }),
  swagger: Joi.boolean().default(false)
})

module.exports = { CreateAccount_schema };

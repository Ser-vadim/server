const bookshelf = require("../db/bookShelf");
require("./User.Model");

const Lot = bookshelf.model("Lot", {
  tableName: "lots",
  card_id: function () {
    return this.belongsTo("Card", "card_id");
  },
  user_id: function () {
    return this.belongsTo("User", "user_id");
  },
});

module.exports = { Lot };

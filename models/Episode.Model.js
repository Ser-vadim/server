const bookshelf = require("../db/bookShelf");

const Episode = bookshelf.model("Episode", {
  hasTimestamps: ['created', 'updated_at'],
  tableName: "episodes",
  cards: function () {
    return this.belongsToMany("Card", "episode_cards", "episode_id", "card_id");
  },
});

module.exports = { Episode };

const bookshelf = require("../db/bookShelf");
// require('./Card.Model')

const Set = bookshelf.model("Set", {
  tableName: "sets",
  cards: function () {
    return this.belongsToMany("Card", "card_sets", "set_id", "card_id");
  },
});

module.exports = { Set };

const bookshelf = require("../db/bookShelf");
// eslint-disable-next-line no-unused-vars
require("chalk");
require("./Episode.Model");
require("./User.Model.js");
require("./Set.Model");

const Card = bookshelf.model("Card", {
  hasTimestamps: ['created', 'updated_at'],
  tableName: "cards",
  location: function () {
    return this.belongsTo("Location", "location");
  },
  episodes: function () {
    return this.belongsToMany(
      "Episode",
      "episode_cards",
      "card_id",
      "episode_id"
    );
  },
  origin: function () {
    return this.belongsTo("Location", "origin");
  },
  cards_user: function () {
    return this.belongsToMany("User", "cards_user", "card_id", "user_id");
  },
});

module.exports = { Card };

const express = require("express");
const Auth_router = express.Router();
const {
    RegistrationAccountAction,
    AuthenticationAction,
    GettingCredentialsAction,
    EnsureTokenAction,
} = require("../../controllers/user/user.controller");
const {validationMiddleware} = require("../../middleware/validation.middleware");
const {CreateAccount_schema} = require("../../validations/Account.validation");

Auth_router.route("/user/registration").post(
    validationMiddleware(CreateAccount_schema, "body"),
    RegistrationAccountAction
);

Auth_router.route("/user/authentication")
    .post(AuthenticationAction);

Auth_router.route("/user/GettingCredentials")
    .get(GettingCredentialsAction)

Auth_router.route("/user/ensureToken")
    .get(EnsureTokenAction);

module.exports = Auth_router;

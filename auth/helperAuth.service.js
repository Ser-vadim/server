require("dotenv").config({ path: `./.env.${process.env.NODE_ENV}` });
const express = require("express");
const app = express();
const crypto = require("crypto");
const jwt = require("jsonwebtoken");
const { addTime } = require("../Helper/functionality_with_time/index");

function randomTokenString(size = 40) {
  return crypto.randomBytes(size).toString("hex");
}

const setTokenCookie = (res, token, amount, unit) => {
  if (app.get("env") === "development") {
    res.cookie("refreshToken", token, {
      httpOnly: true,
      expires: addTime({ amount: amount, unit: unit }),
    });
  } else {
    /*set expire time for dev 1 month*/
    res.cookie("refreshToken", token, {
      sameSite: "none",
      secure: true,
      httpOnly: true,
      expires: addTime({ amount: amount, unit: unit }),
    });
  }
};
const GenerateJwtToken = async (payload, unit) => {
  // create a jwt token containing the account id that expires in 1 day
  return jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, {
    expiresIn: unit,
  });
};

module.exports = {
  randomTokenString,
  setTokenCookie,
  GenerateJwtToken,
};

require("dotenv").config({path: `./.env.${process.env.NODE_ENV}`});
const userRepository = require("../../repositories/User_Repository/user.repository");
const {setTokenCookie} = require("../../auth/helperAuth.service");
const {
    randomTokenString,
    GenerateJwtToken,
} = require("../helperAuth.service");
const Chalk = require("chalk");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const Errors = require("../../CONSTANTS/Errors_const");
const {ErrorGeneration} = require("../../Helper/ErrorGeneration");
const {
    BCRYPT_SALT_ROUNDS,
    LIFE_OF_CONFIRMATION_EMAIL_LINK,
} = require("../../CONSTANTS/Auth_const");
const {addTime} = require("../../Helper/functionality_with_time");
const UnitOfTime = require("../../CONSTANTS/UnitTime_const");
const Mailer = require("../../modules/Mailer/index");
const Cron = require("../../modules/Cron/index");
const appDir = require("../../Helper/getRootDirName")();
const ejs = require("ejs");

const Registration = async (reqBody, res) => {
    const isUserExist = await userRepository.checkingExistUserByEmailAndName(
        reqBody.nickname,
        reqBody.email
    );
    if (isUserExist) {
        throw ErrorGeneration(
            `the user has been taken change email or nickName`,
            Errors.customError,
            200
        );
    }
    const hashedPassword = await bcrypt.hash(
        reqBody.password,
        BCRYPT_SALT_ROUNDS
    );
    const refreshToken = randomTokenString();
    const user = await userRepository.creatingNewUser(
        reqBody,
        hashedPassword,
        refreshToken
    );
    const link = `${process.env.DOM_NAME_FRONT_SIDE}/?userId=${user.id}`;
    const template = await ejs.renderFile(
        `${appDir}/templates/email_teamplates/confirmation_mail.teamplate.ejs`,
        {
            confirmationEmailLink: link,
        }
    );
    await new Mailer().sendMail({
        from: process.env.SENDER_FROM_EMAIL,
        to: user.email,
        subject: "Confirmation email",
        text: "Hello dear client",
        html: template,
    }, async () => {
        await userRepository.deleteUserById(user.id);
    });
    new Cron({
        time: `*/${LIFE_OF_CONFIRMATION_EMAIL_LINK} * * * *`,
        executeOneTime: true,
    }).checkConfirmedEmail(user.id);
    const AccessToken = reqBody.swagger ? await GenerateJwtToken({
        id: user.id,
        role: user.role
    }, "30min") : '';
    if (reqBody.swagger) {
        setTokenCookie(
            res,
            refreshToken,
            2,
            UnitOfTime.days
        );
    }
    return {AccessToken};
};

const Authentication = async (reqBody) => {
    const user = await userRepository.FindByEmail(reqBody.email);
    if (!user) {
        throw ErrorGeneration("user not found", Errors.customError)
    }
    const validatePas = await bcrypt.compare(reqBody.password, user.password);

    if (!validatePas) {
        throw ErrorGeneration("wrong password", Errors.customError)
    }
    const refreshToken = randomTokenString();
    const expires_refreshToken = addTime({amount: 1, unit: UnitOfTime.minutes});
    const accessToken = await GenerateJwtToken({id: user.id, role: user.role}, '30min');
    return {
        newRefreshToken: await userRepository.UpdateRefreshToken(
            refreshToken,
            expires_refreshToken,
            user.id
        ),
        accessToken,
    };
};


const GettingCredentials = async (userId, next) => {
    /*TODO write logick getting credentials*/
};

const EnsureToken = async (accessToken, refreshToken) => {
    try {
        const bearerHeader = accessToken;
        if (typeof bearerHeader !== "undefined") {
            const accessToken = bearerHeader.split(" ")[1];
            await jwt.verify(accessToken, process.env.ACCESS_TOKEN_SECRET);
            return {accessToken: accessToken};
        }
    } catch (error) {
        if (error.message === "jwt expired") {
            const user = await userRepository.FindOneByRefreshToken(refreshToken, ['id', 'role', 'expires_refreshToken_in']);
            if (user.expires_refreshToken_in < Date.now() / 1000) {
                throw ErrorGeneration("refresh_token is expired", Errors.customError);
            } else {
                const NewRefreshToken = randomTokenString();
                const expires_refreshToken = addTime({
                    amount: 1,
                    unit: UnitOfTime.minutes,
                });
                const updatedRefreshToken = await userRepository.UpdateRefreshToken(
                    NewRefreshToken,
                    expires_refreshToken,
                    user.id
                );
                const NewAccessToken = await GenerateJwtToken({id: user.id, role: user.role}, "30min");
                return {
                    accessToken: NewAccessToken,
                    updatedRefreshToken: updatedRefreshToken,
                };
            }
        } else {
            throw ErrorGeneration("permission denied", Errors.customError, 401);
        }
    }
};

module.exports = {
    Registration,
    Authentication,
    EnsureToken,
    GettingCredentials,
};

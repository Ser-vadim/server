const database = require("../../db");
const Chalk = require("chalk");
const moment = require("moment");
const { Episode } = require("../../models/Episode.Model");
const {Location} = require("../../models/location.Model");

const insertEpisodeData = async (episode) => {
  const qb = Episode.query();
  const createdEpisode = await new Episode({
    name: episode.name,
    air_date: episode.air_date,
    episode: episode.episode,
  }).save();
  await database("episodes")
    .update({ url: `${process.env.DOM_NAME}/api/episode/${createdEpisode.id}` })
    .where({ id: createdEpisode.id });
  await insert_episode_cards(episode.characters, createdEpisode.id);
  const episodeWithRelations = await getEpisodeById(createdEpisode.id);
  return episodeWithRelations;
};

async function insert_episode_cards(ids_cards = [], eposode_id) {
  const attached = await new Episode({ id: eposode_id })
    .cards()
    .attach(ids_cards);
  return attached;
};

async function checkDuplicateEpisode(select = "*", fieldSearching) {
  const qb = Episode.query();
  const isDuplicated = await qb.select(select).where(fieldSearching);
  return !!isDuplicated.length;
};

async function getEpisodeById(episode_id){
  const card = await Episode.where({id: episode_id}).fetch({withRelated: ['cards']})
  return card.toJSON();
};

module.exports = {
  insertEpisodeData,
  insert_episode_cards,
  checkDuplicateEpisode,
  getEpisodeById
};

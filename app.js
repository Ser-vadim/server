require("dotenv").config({path: `./.env.${process.env.NODE_ENV}`});

const http = require("http");
const express = require("express");
const socket = require("socket.io");
const app = express();
const server = http.createServer(app);
const swaggerUi = require("swagger-ui-express");
const swaggerDoc = require('./swagger/swagger.json')
const io = socket(server, {
    cors: {
        origin: "*",
    },
});
const initListeners = require("./socket/listeners");
const PORT = process.env.PORT || 3001;
let cors = require("cors");
let cookieParser = require("cookie-parser");
const Chalk = require("chalk");
const Auth_router = require("./routes/common/common-auth.routes");
const Admin_card_router = require("./routes/admin/admin-card.routes");
const Admin_location_router = require("./routes/admin/admin-location.routes");
const Admin_episode_router = require("./routes/admin/admin-episode.routes");
const Common_Card_router = require("./routes/common/common-card.routes");
const {errorHandler} = require("./middleware/handler_Error");
const {checkJwtMiddleware} = require("./middleware/checkJwt.middleware");
// const testFs = require('./Helper/gettingRoutes')();
const gettingRoutes = require('./Helper/gettingRoutes');
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());
app.use(cookieParser());
app.use("/", express.static("./public/uploaded_Image_Character"));
app.use(checkJwtMiddleware);

app.use('/api-docs', async (req, res, next) => {
    swaggerDoc.servers = [{
        "url": `http://localhost:${PORT}/api/`,
        "description": "development"
    }]
    req.swaggerDoc = swaggerDoc;
    next()
}, swaggerUi.serve, swaggerUi.setup());


app.use("/api", Auth_router);
app.use("/api", Admin_card_router);
app.use("/api", Admin_location_router);
app.use("/api", Admin_episode_router);
app.use("/api", Common_Card_router)
app.use(errorHandler);

app.listen(PORT, () => {
    console.log(Chalk.hex("#0388fc").italic(`Server run at port ${PORT}`));
});

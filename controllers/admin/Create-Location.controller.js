const {
  creatingLocation,
} = require("../../services/admin/Creating-Location.service");
const Chalk = require("chalk");

const createLocationAction = async (req, res, next) => {
  try {
    const createdLocation = await creatingLocation(req.body);
    res.status(200).json({ info: "created the location", location: createdLocation });
  } catch (error) {
    next(error);
  }
};

module.exports = {
  createLocationAction,
};

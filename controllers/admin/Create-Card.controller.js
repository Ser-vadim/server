const { creatingCard } = require("../../services/admin/Creating-Card.service");
const Chalk = require("chalk");

const CreatCardAction = async (req, res, next) => {
  try {
    const createdCard = await creatingCard(req.body);
    res.status(200).json({ info: "created the card", card: createdCard });
  } catch (error) {
    next(error);
  }
};

module.exports = { CreatCardAction };

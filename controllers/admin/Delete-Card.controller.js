const { deletingCard } = require("../../services/admin/Deleting-Card.service");

const deleteCardAction = async (req, res, next) =>{
    try{
        const {id} = req.params;
        await deletingCard(id);
        res.status(200).json({ info: "card success deleted" });
    }catch (error){
        next(error)
    }
}

module.exports = {deleteCardAction}
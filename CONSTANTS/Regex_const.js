const REGEX_NOT_ONLY_NUMBER =
  /^[0-9]*[a-zA-Z]+[a-zA-Z0-9].*$/; /*name should not contain only number and any any special characters*/
const REGEX_PASSWORD =
  /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d].{8,100}$/; /*password should be least 8 characters long and contain numbers and one capital letter*/

const REGEX_SWAGGER_ROUTER = /^\/api-docs.*/;
const REGEX_NAME_DIRECTORY = /\w+(?=\\\w+[.|-]\w+.\w+.js)/;

module.exports = {
  REGEX_PASSWORD,
  REGEX_NOT_ONLY_NUMBER,
  REGEX_SWAGGER_ROUTER,
  REGEX_NAME_DIRECTORY,
};

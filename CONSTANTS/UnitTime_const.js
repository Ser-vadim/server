const UnitOfTime = {
  minutes: "minutes",
  days: "days",
  months: "months",
  weeks: "weeks",
  hours: "hours",
  years: "years",
};

module.exports = UnitOfTime;

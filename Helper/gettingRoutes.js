const {resolve} = require('path');
const {readdir} = require('fs').promises;
const {REGEX_NAME_DIRECTORY} = require('../CONSTANTS/Regex_const');
const directoryListing = {}


async function* gettingFilesFromDirectory(dir) {
    try {
        let dirents = await readdir(dir, {withFileTypes: true});
        for (const dirent of dirents) {
            const res = resolve(dir, dirent.name);
            if (dirent.isDirectory()) {
                directoryListing[dirent.name] = []
                yield* gettingFilesFromDirectory(res);

            } else {
                const matched = res.match(REGEX_NAME_DIRECTORY)
                directoryListing[matched[0]].push(res)
                yield directoryListing;
            }
        }
    } catch (error) {
        console.log(error);
    }

}


const workWithAsync = async (directory) =>{
    let generator = await gettingFilesFromDirectory('C:/Any_projects/auction/server/routes');
    let result = null;
    for await (let value of generator){
        result = value;
    }
    console.log(result)
}
workWithAsync()


module.exports = gettingFilesFromDirectory

// const {Admin_card_router} = require("../routes/admin/admin-card.routes");
// const {Admin_location_router} = require("../routes/admin/admin-location.routes");
// const {Admin_episode_router} = require("../routes/admin/admin-episode.routes");
// const {Common_Card_router} = require("../routes/common/common-card.routes");
// return {
//     Admin_card_router,
//     Admin_location_router,
//     Admin_episode_router,
//     Common_Card_router,
// }
const errorGeneration = (message, name, status) => {
  let error = new Error(message);
  error.name = name;
  error.status = status;
  return error;
};
module.exports = {
  ErrorGeneration: errorGeneration,
};

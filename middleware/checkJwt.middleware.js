require("dotenv").config({ path: `./.env.${process.env.NODE_ENV}` });

const jwt = require("jsonwebtoken");
const userRepository = require("../repositories/User_Repository/user.repository");
const {
  randomTokenString,
  GenerateJwtToken,
} = require("../auth/helperAuth.service");
const Chalk = require("chalk");
const { addTime } = require("../Helper/functionality_with_time/index");
const UnitOfTime = require("../CONSTANTS/UnitTime_const");
const NoneprotectedRouters = require("../CONSTANTS/noneProtectedRouters");
const { ErrorGeneration } = require("../Helper/ErrorGeneration");
const Errors = require("../CONSTANTS/Errors_const");
const { REGEX_SWAGGER_ROUTER } = require("../CONSTANTS/Regex_const");


const checkJwtMiddleware = async (req, res, next) => {
  if (
    NoneprotectedRouters.includes(req.path) ||
    REGEX_SWAGGER_ROUTER.test(req.path)
  ) {
    return next();
  }
  try {
    const accessToken = req.headers["authorization"]
      ? req.headers["authorization"].split(" ")[1]
      : null;
    if (!accessToken) {
      throw ErrorGeneration("permission denied", Errors.customError, 401);
    }
    jwt.verify(accessToken, process.env.ACCESS_TOKEN_SECRET, (error, _) => {
      if (error && error.message === "jwt malformed") {
        throw ErrorGeneration("permission denied", Errors.customError, 401);
      }
      if(error &&  error.message === 'jwt expired'){
        throw ErrorGeneration("jwt expired", Errors.customError, 401);
      }
    });
    next();
  } catch (error) {
    next(error);
  }
};

/*logic for checking jwt token*/
// const refreshToken = req.cookies.refreshToken;
// try {
//   const bearerHeader = req.headers["authorization"];
//   if (refreshToken === undefined) {
//     throw "expired cookies";
//   }
//   if (typeof bearerHeader !== "undefined") {
//     const bearer = bearerHeader.split(" ")[1];
//     await jwt.verify(bearer, process.env.ACCESS_TOKEN_SECRET);
//     next();
//   }
// } catch (error) {
//   if (error.message === "jwt expired") {
//     let { id, expires_refreshToken_in } =
//         await userRepository.FindOneByRefreshToken(refreshToken);
//     if (expires_refreshToken_in < Date.now() / 1000) {
//       throw "refresh_token is expired";
//     } else {
//       const refreshToken = randomTokenString();
//       const expires_refreshToken = addTime({amount: 1, unit: UnitOfTime.minutes});
//       const updetedEntities = await userRepository.UpdateRefreshToken(
//           refreshToken,
//           expires_refreshToken,
//           id
//       );
//       const accessToken = await GenerateJwtToken(id);
//       req.accessToken = accessToken;
//       next();
//     }
//   } else {
//     next(creatingError("permission denied", Errors.customError, 401 ));
//   }
// }
module.exports = {
  checkJwtMiddleware,
};

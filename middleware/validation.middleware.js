const Joi = require("joi");

const validationMiddleware = (
  schema,
  property,
  optionsValidation = {
    abortEarly: true /* abort first error */,
    allowUnknown: true /* ignore unknown props */,
    stripUnknown: true /* remove unknown props */,
  }
) => {
  return (req, res, next) => {
    const { error, value } = schema.validate(req[property], optionsValidation);
    const valid = error == null;

    if (valid) {
      req.body = value;
      next();
    } else {
      return next(error);
    }
  };
};

module.exports = { validationMiddleware };

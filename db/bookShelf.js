const Knex = require("./index");
const bookshelf = require("bookshelf")(Knex);

module.exports = bookshelf;

const crypto = require("crypto");
const {addTime} = require("../../Helper/functionality_with_time");
const bcrypt = require("bcrypt");
const {
    BCRYPT_SALT_ROUNDS,
} = require("../../CONSTANTS/Auth_const");
const UnitOfTime = require("../../CONSTANTS/UnitTime_const");

exports.seed = function (knex) {
    return knex("users").then(async function () {
        // Inserts seed entries
        return knex("users").insert({
            nick_name: "Vadym_JSN",
            password: await bcrypt.hash('123456', BCRYPT_SALT_ROUNDS),
            email: "vadym.serdiuk.jsn@gmail.com",
            refresh_token: crypto.randomBytes(40).toString("hex"),
            expires_refreshToken_in: addTime({amount: 30, unit: UnitOfTime.days}),
            rating_range: 0,
            cash_balance: 0,
            role: 'admin',
            is_email_confirm: true,
            avatar_url: null,
        });
    });
};

const axios = require("axios");

const getResidents = async () => {
  let totalPages = 6;
  let residents = [];
  for (let i = 1; i <= totalPages; i++) {
    let {
      data: { results },
    } = await axios.get(`https://rickandmortyapi.com/api/location?page=${i}`);

    residents.push(...results);
  }
  return residents;
};

exports.seed = async function (knex) {
  const locations = await getResidents();
  const location_residents = [];
  let objResidents = {};

  for (let i = 0; i < locations.length; i++) {
    if (!locations[i].residents.length) {
      objResidents = {
        card_id: null,
        location_id: locations[i].id,
      };
      location_residents.push(objResidents);
    }
    for (let j = 0; j < locations[i].residents.length; j++) {
      const card_id = await knex("cards")
        .where({ url: locations[i].residents[j] })
        .select("id");
      objResidents = {
        card_id: card_id[0].id,
        location_id: locations[i].id,
      };
      location_residents.push(objResidents);
    }
  }
  return knex("location_residents").insert(location_residents);
};

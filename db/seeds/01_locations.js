const axios = require("axios");

const getAllLocations = async () => {
  let totalPages = 6;
  let locations = [];
  for (let i = 1; i <= totalPages; i++) {
    let {
      data: { results },
    } = await axios.get(`https://rickandmortyapi.com/api/location?page=${i}`);
    let _locations = results.map((location) => {
      delete location.residents;
      delete location.id;
      return location;
    });

    locations.push(..._locations);
  }
  return locations;
};

exports.seed = async function (knex) {
  const locations = await getAllLocations();
  return knex("locations").insert(locations);
};

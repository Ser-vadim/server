const environment = process.env.NODE_ENV || "development";
const config = require("./knexfile")[environment];
const Knex = require("knex")(config);

module.exports = Knex;

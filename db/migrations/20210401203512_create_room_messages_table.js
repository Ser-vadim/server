exports.up = function (knex, Promise) {
  return knex.schema.createTable("room_messages", (table) => {
    table.integer("room_id").unsigned();
    table.integer("message_id").unsigned();

    table.foreign("room_id").references("id").inTable("rooms").onDelete('CASCADE');
    table.foreign("message_id").references("id").inTable("messages").onDelete('CASCADE');
  });
};

exports.down = function (knex, Promise) {
  return knex.schema.dropTable("room_messages");
};

exports.up = function (knex, Promise) {
  return knex.schema.createTable("episode_cards", (table) => {
    table.integer("episode_id").unsigned();
    table.integer("card_id").unsigned();

    table.foreign("episode_id").references("id").inTable("episodes").onDelete('CASCADE');
    table.foreign("card_id").references("id").inTable("cards").onDelete('CASCADE');
  });
};

exports.down = function (knex, Promise) {
  return knex.schema.dropTable("episode_cards");
};

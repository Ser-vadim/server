exports.up = function (knex, Promise) {
  return knex.schema.createTable("location_residents", (table) => {
    table.integer("location_id").unsigned();
    table.integer("card_id").unsigned();

    table.foreign("location_id").references("id").inTable("locations").onDelete('CASCADE');
    table.foreign("card_id").references("id").inTable("cards").onDelete('CASCADE');
  });
};

exports.down = function (knex, Promise) {
  return knex.schema.dropTable("location_residents");
};

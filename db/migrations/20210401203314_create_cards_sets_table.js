exports.up = function (knex, Promise) {
  return knex.schema.createTable("card_sets", (table) => {
    table.integer("set_id").unsigned();
    table.integer("card_id").unsigned();

    table.foreign("set_id").references("id").inTable("sets").onDelete('CASCADE');
    table.foreign("card_id").references("id").inTable("cards").onDelete('CASCADE');
  });
};

exports.down = function (knex, Promise) {
  return knex.schema.dropTable("card_sets");
};

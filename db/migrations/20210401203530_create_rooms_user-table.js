exports.up = function (knex, Promise) {
  return knex.schema.createTable("rooms_users", (table) => {
    table.integer("room_id").unsigned();
    table.integer("user_id").unsigned();
    table.foreign("room_id").references("id").inTable("rooms").onDelete('CASCADE');
    table.foreign("user_id").references("id").inTable("users").onDelete('CASCADE');
  });
};

exports.down = function (knex, Promise) {
  return knex.schema.dropTable("rooms_users");
};

exports.up = function (knex, Promise) {
  return knex.schema.createTable("lots", (table) => {
    table.increments("id").primary();
    table.integer("card_id").unsigned();
    table.integer("user_id").unsigned();
    table.integer("init_rate");
    table.integer("min_step");
    table.integer("max_step");
    table.integer("price");
    table.integer("max_duration");
    table.integer("min_extention");
    table.boolean("is_done_auction");
    table.boolean("is_admine_lot");
    table.integer("initial_rating");
    table.timestamp("created_at").defaultTo(knex.fn.now());

    table.foreign("card_id").references("id").inTable("cards").onDelete('CASCADE');
    table.foreign("user_id").references("id").inTable("users").onDelete('CASCADE');
  });
};

exports.down = function (knex, Promise) {
  return knex.schema.dropTable("lots");
};

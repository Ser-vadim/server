exports.up = function (knex, Promise) {
  return knex.schema.createTable("locations", (table) => {
    table.increments("id").primary()
    table.string("name");
    table.string("type");
    table.string("dimension");
    table.string("url");
    table.string("created");
    table.string("updated_at");

  });
};
exports.down = function (knex, Promise) {
  return knex.schema.dropTable("locations");
};

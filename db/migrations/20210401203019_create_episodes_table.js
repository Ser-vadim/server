//TODO make column name "created" as data not string

exports.up = function (knex, Promise) {
  return knex.schema.createTable("episodes", (table) => {
    table.increments("id").primary();
    table.string("name");
    table.string("air_date");
    table.string("episode");
    table.string("url");
    table.string("created");
    table.string("updated_at");
  });
};

exports.down = function (knex, Promise) {
  return knex.schema.dropTable("episodes");
};

exports.up = function (knex, Promise) {
  return knex.schema.createTable("messages", (table) => {
    table.increments("id").primary();
    table.string("message");
    table.integer("user_id").unsigned();
    table.timestamp("created_at").defaultTo(knex.fn.now());

    table.foreign("user_id").references("id").inTable("users").onDelete('CASCADE');
  });
};

exports.down = function (knex, Promise) {
  return knex.schema.dropTable("messages");
};
